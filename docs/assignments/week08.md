# 8. Embedded Programming

In this week I started programming my designed pcb. Therefor I needed an Arduino uno, a ftdi cable, jumper wires and a usb cable to connect the arduino to my laptop.

## Preparations

To be able to upload a programm to my chip I first had to open the AurduinoISP Sketch (can be found under File>Examples>ArduinoISP). Then I connected the Arduino to my Laptop and selected the correct port, board and programmer unter "tools" (COM6 to me, Arduino uno and Arduino as ISP). Then I uploaded the sketch to the Arduino and disconnected it.  
In the next step I hab to connect the Arduino uno to my pcb. Therefor I used th follwing image as an orientation:

<br>

![](../images/week08/arduino_connection.png)

<br>

I conected the jumper wires to the Arduino as it is shown in the picture. But I had a different pcb, so I looked up the datasheet for my microcontroller (attiny84), and looked for the pins (had so search for the pins MISO, MOSI, RST,VCC and SCK). 

<br>

![](../images/week08/Inkeddatasheet.jpg)

<br>

As shown in the image above, I had to connect the Arduino GND to microchip pin 14, Arduino VCC to microchip 1, Arduino pin 10 to microchip pin 4 (PB3), Arduino pin 11 to microchip pin 7 (PA6), Arduino pin 12 to microchip pin 8 (PA5) and Arduino pin 13 to microchip pin 9 (PA4). 

<br>

![](../images/week08/arduinocon.jpg)

<br>

As soon as it was connected, I selected again the correct port, board, processor and clock under "Tools" (COM6 to me, Attiny 24/44/84, Attiny84, 8MHz). Then I selected "Burn bootloader" under "Tools".   
Now I could write the Code and upload it with programmer later. For that I wrote an array which contains 0 and 1. This array should be read and the LED should be High (blink) when it reads a 1 and Low when it reads 0.
The Code looked like this:

<br>

![](../images/week08/code.png)
![](../images/week08/loop.png)

<br>

First I had to include the SoftwareSerial Library, so I could use the serial monitor. Then I defined the pins. I needed RX and TX because I wanted to use an ftdi cable to transmit data from board to pc. In the void setup I defined the communication begin and setted the LED as an output. In the loop I counted from 0 to 8 (array length) and in every count it was checked if the array at this position was 0 or 1. It would set the LED on HIGH when it was 1 and LOW when it was 0.  
Because I couldnt use my LED which was at the pcb, I had to include an extern LED. For that I needed a circuit board, where I connected the LED with a resistor and - and +. The connection looked like this:  

<br>

![](../images/week08/20211124_155201.jpg)

<br>

The minus pool had to be connected to GND and the plus Part to a free Pin. Now, the LED was cnnected to the Chip. In the last step I just had to connect the pcb to the laptop without the Arduino. Therefor I used the ftdi cable. It has 6 connections, but I only used four. I connected the black one with GND, the red one with +5V, the orange one (RX) to the free pin (I defined as RX in the Code) and the yellow one (TX) with the free pin (I defined as TX).

<br>

![](../images/week08/20211124_123224.jpg)  
![](../images/week08/20211124_155204.jpg)  

<br> 

As last step I connected the ftdi cable with the laptop. The pcb had power and a connection to it. So it executed the code automatically and printed the LED status at the Serial Monitor.



## Download

[Download the Code here](../images/week08/week08.zip)  
