# 5. CNC Mill

This week I worked with the CNC-Mill.Therefor I used a foam block, so I created a 3D Model of the foam and how it should look like. 

## Sketch 

I worked with Fusion360 and started with a rectangle, which I extruded. Then I added a few block and 3D like features, so my sketch looked like this:

<br>

![](../images/week05/Screenshot386.png)

<br> 

The blue lines show the sketch behind the 3D Model. So after sketching the Model, I started with the SetUps for the project. In the SetUps I defined the cutting area. It firstly looked like this: 

<br>

![](../images/week05/Screenshot387.png)

<br>

After that I made the heigth a little bit smaller, so it would have a smaller cutting area. Then I simulated the cutting process:

<br>

![](../images/week05/Screenshot390.png)

<br>

I used Adaptive Clearing  (6mm flat end mill) as a cutting method, because in the sketch there wasn't smaller pieces, so 6 mm would be fine. The Settings for the adaptive clearing were:

<br>

![](../images/week05/adap.png)  

<br>

The model looked like this:  

<br>  

![](../images/week05/Screenshot389.png)

<br>

Also I hab to choose the drill piece of the Cutter in Fusion, so I selected the one I needed:  
<br>

![](../images/week05/Screenshot388.png)

<br> 

Then I measured the single parameter of this piece and set them up:  
(You can see the Parameters in the Picture)

<br>

![](../images/week05/para.jpg)

<br>

You can see, in the Simuation sketch on the left, that the cutting process starts a little bit out of the actual foam piece.So you have to set the origin point in the maschiene dependent to this.

<br>

![](../images/week05/Screenshot391.png)

<br>


## Cutting

The cutting process is similar to the PCB Cutting process last week. I attached the foam piece to the mill and set the XY Origins to the bottom left. It was important that the origins were half a length of the drill beneath the foam, like you could see in the simulation above:

<br>

![](../images/week05/setupmill.jpg)

<br> 

Then I uploaded my file and started cutting. The rpm had to set to max (15000) and the speed was 100%. Then It started cutting: 
<br>

[Download here](../projects/1002.prn)  

<br>

![](../images/week05/cutting.jpg)

<br>

It took a while, and in the first try the foam block went of the maschiene. I had to use a new foam block, attached the block with better tape and restart my try:

<br>

![](../images/week05/miss.jpg)

<br>

 This time it worked and the final product looked like this:

<br>

![](../images/week05/end1.jpg)  
![](../images/week05/end2.jpg)


<br>


## Downloads

<br>

[Download here](../projects/1002.prn)  
[Download Fusion File here](../projects/cnc.f3d)  


