# 3. Laser Cutter

This week I worked with the Laser Cutter. I cutted the sketch I made last week, for the first design of the box for the endproject.

## Sketch and Design

I started with the Sketch for my Box in Fusio360. First I sketched the structure  from the single boxparts, then I added the Joints. Therefor I used a Parametric Design, so  I could set the specific Parameters for the Depth of the Joints (3mm) and the width/heigth etc.
<br>

![](../images/week03/Design.png)

See the single parts down here:

[Download here](../projects/part01.dxf)  
[Download here](../projects/part02.dxf)  
[Download here](../projects/part03.dxf)  
[Download here](../projects/part04.dxf)  
[Download here](../projects/part05.dxf)  
[Download here](../projects/part06.dxf)  
[Download here](../projects/part07.dxf)  

## Material
After finishing the sketch I needed to choose a material for the box. In the sketch I expected a material with 3mm thickness, so I chose wood (it hab 3mm thickness and also it shouldn´t be transparent).
<br>

## Working with the LaserCutter

To cut the Boxparts, I first had to upload my Sketches to the pc files of the Laser Cutter. I had to make a few Setting, which components should be cutted first etc (first I cutted the inner components, then the outer ones, so th part wouldn´t  come of its position). Then I had to set the laserpoint in the maschine to its origin (top left), so it would start cutting from that point/ it takes that point as origin. 
After putting the Material inside, I had to determine the distance between the cutterpart and the material. 
The Setup part looked like this: 
<br>

![](../images/week03/20211027_122720(0).jpg)

At first I cutted the box out of cardboard, to test the different parameters and the lines in the sketch (avoid double lines etc.). Therefor I chose "100" for "power" and "75" for "speed". 
The Endproduct looked like this:  
<br>

![](../images/week03/20211020_132033.jpg)

<br>  

During the cutting process it started to burn, because of the double lines in the sketch...
But the rest worked fine! So after finishing the cardboard prototype and fixing the sketch, I used the wood for the first version of my box:  

<br>  

The cutting process:  

<br>  

![](../images/week03/20211020_134848.jpg)

<br>

The cutted boxparts:  

<br>

![](../images/week03/20211020_135608.jpg)

<br>

Then I interlocked them:

<br>

![](../images/week03/20211020_135734.jpg)

<br>

The prototype on the left, the final product on the right:  

<br> 

![](../images/week03/20211020_140155.jpg)
![](../images/week03/20211020_140220.jpg)
![](../images/week03/20211020_140212.jpg)