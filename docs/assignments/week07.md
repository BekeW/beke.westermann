# 7. Electronics design

## Import Libraries  

To design my own pcb, I used KiCad. In the first steps I imported the FAB Library and the eagle Library. Therefor I opened KiCad, and selected "Preferences" and opened the "Symbol Libraries".  
In this Library I added a empty row table in the Global Libraries and chose the fab.lib file from the downloaded KiCad Project, named it "FAB" and added the Path to it. I repeated this step in Preferences > Footprint Library for the fab.pretty file. Then I created a new folder in the downlaoded Fab-Project, put the con-amp-quick.lbr File in it an added it again to the Footprint Library.  

<br>

![](../images/week07/import1.png)  
![](../images/week07/import2.png)

<br>

## PCB Design - Layout 

To design the pcb, I selected the schematic Layout editor and started creating. I first had to choose a microcontroller. I wanted to use the Attiny44A. It had two important connections: GND and VCC. I added a Power_GND and connected it to the attinys GND. To the VCC I connected the Power of +5V. I also connected the +5V to the PA0 as a reference voltage. The second part was the addition of a capacitor to be able to generate a frequency. I connected it to GND and +5V.  
I also added a resistor with 10k to the +5V connection and a RST. 
Then I connected the LED. Therefor I chose a free PB (in this chase PB0) to connect it and added a Resistor (499) and also connected it to GND.  
In the last step od the design I appended the pins. I needed one 1x04 pin, for the GND and the +5V. I actually just needed 1x02, but I made it double to be sure if one of them would break e.g. during soldering.  
I also needed a 1x05 pin for the addition of PA3, PA4, PA5, PA6 and RST.  
The last pin I needed was 1x03 for the external temperature sensor. The sensor had three connections: GND, VCC an an Output. So I chose these connections.  
All the parts were selected from the library, trough clicking ob the "place symbol" button.

<br>

![](../images/week07/Layout.png)  

<br>


## Assign PCB Footprint to schematic symbols

As said (or written) in the title, the next step was assigning the pcb footprints. Therefor I clicked on the symbol in the menu (sixth symbol next to the bug) and selected the FAB Library on the left:  

<bR>

![](../images/week07/menu.png)  

<br>

Then I searched for the different parts that I needed, double clicked them and pressed "ok" once I finished.  

<br>

![](../images/week07/lib.png)  

<br>

## PCB Layout editor - connection the pcb  

The last step of designing was editing the connections and change the position of the single parts on the pcb. The given connections were marked trough thin white lines. After moving the Parts to a good position, I had to "redraw" the lines with the "Route tracks" tool (on the rigth). It connected automatically when I clicked on the parts, but it was important that the lines didn't overlap each other or were too close to other parts and lines.  
When I finished, it looked like this:  

<br>

![](../images/week07/layout_lines.png)  

<br>

## Exporting the pcb Design  

To Cut the designed pcb, I firstly had to export it as svg file:  

<br>

![](../images/week07/exportkicad.png)  

<br>

In the next step I opened the svg File in Inkscape and exported it as png file. 
[Download here](../projects/Fundamentals-brd.svg)  

<br>

The exported png Image:  

<br>

![](../images/week07/g432.png)  

<br>

After these steps, i opened the above png Image in GIMP. For the margin, I changed the settings of the Canvas size. I added 3 mm on the top/bottom and on the sides. Then I prepared the image for the needed three parts (traces, holes and cutout). I changed the color of the traces and the holes to black, so I just had a white margin. I saved this as png File as cutout.  

<br> 

![](../images/week07/outline.png)  

<br>

I repeated this step. I turned the color of the cutout and the holes into white for the traces:  

<br>

![](../images/week07/traces.png)  

<br>

Same with the holes: 

<br>

![](../images/week07/holes.png)  

<br>

Last step was setting up the parameters for the cutting process in fabmodules.org  
Here I selected the input format (png image), the output format (rml file), and set the rest. For the traces I had to invert the image and select "PCB traces (1/64)" with x0, y0, z0 all to 0, cut depth to 0, offset number to 8. For the cutout and the holes, i also had to invert the images an set "PCB outline (1/32)", cut depth to  
0,5 and press on "calculate". For all three Setups, I had to select "MDX-40" as machiene.  

<br>

![](../images/week07/traces_con.png)  

<br>

## Cutting  

For the cutting process I put the pcb material into the machiene and attached it with tape. I swaped the endmill to the one with 3,175 mm. Then I set the x und y axis as origin and after that the z axis. Therefor I went down on the axis step for step until the endmill touched the pcb. Once it was set, I selected the file (the traces first, then the holes and the coutout at the end) and started the cutting process.
After the first cutting process, I changed the endmill (now changed to 3,9688 mm) and started the cutting process for the holes. I repeated this step for the cutout. (For more details go to the week04 project)


## Soldering  

For the soldering process I selected the parts I needed (I had to define them a few steps earlier in the pcb design with KiCad)

<br> 

![](../images/week07/parts.jpg)  

<br>

I started with the microchip, then I soldered the rest of the parts. (For more details got to week 04 again)  

<br>

After soldering I connected the board to the arduinoand bunred it as bootloader. I had no error message and the LED was blinking:

<br>

![](../images/week08/20211124_155204.jpg)  



## Downloads  

[Download the source file here](../projects/kicad-master.zip)  
[Download the holes here](../projects/holes(1).rml)  
[Download the cutout here](../projects/outline.rml)  
[Download the traces here](../projects/traces(2).rml)  







