# 6. 3D Printing

## 3D Model

For the 3d Printer assignment this week, I printed a tealight for a candle. The 3d Model was made with Fusion360. Therefor I started the sketch with a sideview of the basic model. So I created a circle (the tealight should have a round form) and removed the parts I didnt need.


![](../images/week06/skecth1.png)  
![](../images/week06/sketch.png)  

<br> 

So after this I had the sideview. In the next step I rotated the sketch around the y-Axis and got the basic vessel for the tealight:  

<br>
(Menu)

![](../images/week06/menu.png)  

<br>

(Top view)  

![](../images/week06/Top.png)  

<br>

After this, I added a net structure, so I had to transform the normal body into a body with net structure:

<br>

(Menu)  

![](../images/week06/menu2.png)  

<br>

With the following Settings:  

<br>

![](../images/week06/netzmenu.png)  


<br>

(The new body)  

![](../images/week06/netzansicht.png)  
![](../images/week06/netzansicht2.png)  

<br>

The next Steop was reducing the netz, so it would look like a more random structure, which I could use as vessel at the end:  

<br>

(Reduce menu in Fusion)  
![](../images/week06/menu3.png)  

<br>

(New net structure)  
![](../images/week06/netzansicht3.png)  

<br>

After this I converted the net body back to normal:  

<br>

![](../images/week06/menu4.png) 

<br>

So it looked like this:  

<br>

![](../images/week06/konvertedNetz.png)  

<br>

Now I used the tube function to create the new body, which was going to be the tealight. Therefore I selected the inner lines and transformed them into tubes:  

<br>

![](../images/week06/tubeansicht.png)  

![](../images/week06/maketubes.png)  

<br>

I chose 1mm as tube diameter, so I got the wished structure:

<br>

![](../images/week06/tubeshinzugefuegt.png)  

![](../images/week06/tubeshinzugefuegt2.png)  

<br>

The finished tealight body looked like this:  

<br>

![](../images/week06/body3.png)  

In the next steps, I used the shown tool to change the holes in the body:

<br>

![](../images/week06/begrenzung.png)  
![](../images/week06/begrenzung2.png)  

<br>

After this step th new body looked like this:

<br>

![](../images/week06/body5.png) 
![](../images/week06/body6.png) 

<br>

In the last step i added a cylinder as bottom of the tealight: 

<br>

![](../images/week06/zylinder.png) 
![](../images/week06/zylinderansicht.png) 

<br>

As last step I exported the model as a .stl File and opened it in Ultimaker Cura:

<br>

![](../images/week06/ultimaker1.png) 

<br>

Here I choosed the printer I wanted to use (Ultimaker S5) and had a few Settings to make:  

<br>

![](../images/week06/s1.png) 
![](../images/week06/s2.png) 
![](../images/week06/s3.png) 

<br>

I used the support for the bottom side, so it wouldn't break in. And I used less bottom layers, because I wouldn't need that much. There I used more top layers, to be safe it keep its structure. 

<br>

![](../images/week06/su.png) 
![](../images/week06/end.png) 

## Printing

After slicing the model, I saved it on the stick and put it in the 3d printer. I selected the file and started printing. 

<br>

![](../images/week06/20211110_130556.jpg) 

<br>

The final printed tealigth:

<br>

![](../images/week06/endmitsupp.jpg) 


<br>

After cutting away the support:

<br>

![](../images/week06/endmitkerze.jpg)
![](../images/week06/endmitkerze2.jpg)

<br>

## Download  

[Download stl here](../projects/3D_Drucker_Teelicht_v5.stl)  
[Download Fusionfile here](../projects/Teelicht.f3d)  
[Download ufp here](../projects/UMS5_3D_Drucker_Teelicht_black_v6.ufp)  







