# 4. PCB - Electronics

The work with electronics in this week was based on a given file, we had a predesigned pcb part.


## Preparing the files from FreeCad

 In the first steps I exported every part (traces, holes and the cutout) from the given folder (therefor I hab to open the file "hello.kicad_pcb"), deactivated the labels, created an svg-file and exported them as a png into Inkscape. There I had to divide the image in three different parts, so the maschine would engrave the traces and cut the holes and the outer lines out. Therefor I changed the colors of the holes/traces/cutout to white/black (depending on what I needed, the white parts would be cut out) in Gimp. So, I turned everything into black except e.g. the holes, to have a file for the holes-cutout. I repeated the same with the outer cutout and the traces. Then I exported all three images as png files.

In the next step I setted up the parameters for the cutting process in fabmodules.org (here I prepared the files for cutting and converted them into useable files for the machiene):

<br>

![](../images/week04/traces.png)  
![](../images/week04/holes.png)

<br> 

There were different parameters for echt part (for the traces and the holes/cutout).
After exporting the Files for the RolandMill, I took the different Components that were given in the Files:
<br> 

![](../images/week04/20211027_120154.jpg)

<br> 

Next step was cutting the pcb with the RolandMill. Therefor I placed the material in the maschine, activted the vacuum (so it wouldnt move while cutting) and set the x and y coordinates for the origin. Then I activated the spining and the the z coordinate. It just had to touch the material, so it would cut. The last step was uploading the file (first the traces, then the holes and th cutout at the end) and press start. It took a while, but when it finished, I leaned it up with Aceton and went to the next step:
<br>
The cutting process:
<br>

![](../images/week04/20211027_125645.jpg)  

<br>

![](../images/week04/20211027_125404.jpg)  

<br>

For the different cuttings, I had to use different components (3,9688 for the cutout and holes, 3,175 for the traces):
<br>

![](../images/week04/20211027_122756.jpg)

<br>

The cutted pcb looked like this: 

<br>

![](../images/week04/20211027_132633.jpg)

<br>

## Soldering

The soldering was a little bit tricky, the components were very small and had to be solded to their exact position. I started with the processor, it was the most important part of the pcb, then I soldered the rest of the components, the resistors, the LED etc. At the end the pcb was ready to be tested.

<br>

![](../images/week04/20211027_160036.jpg)

<br>

## Testing the pcb

To test the pcb I used an Arduino. After installing the Arduino IDE and changing the setting (Board/Processor/Frequency -> attiny25/45/85, attiny45, internal 8mhz and select the Arduino as ISP as the programmer). Then I connected the Arduino with my pcb and tested it (hab to select "burn Bootloader"):

<br>
(The connections which were given: )
<br>

![](../images/week04/arduino_connection.png)  

<br>
The connected pcb, with blinking LED:  

<br>

![](../images/week04/20211027_165201.jpg)

<br>

In the first few try it didn´t blink, so I tested the single connections and had to re-solder some of the parts. After the little improvement the LED blinked and it worked!