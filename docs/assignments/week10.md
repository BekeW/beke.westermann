# 10. Output Devices

This week I worked with an Ouput Device and my own PCB. As an Output Device I used a speaker (with 8 Ohm and 0.5w). I used the same Arduino as last week, so I didnt had to upload the ArduinoISP sketch or burn the pcb as bootloader. So I just had to upload the code. Therefor I connected the PCB to the Arduino as usual (the Arduino GND to microchip pin 14 (black), Arduino VCC to microchip 1 (red), Arduino pin 10 to microchip pin 4 (green), Arduino pin 11 to microchip pin 7 (yellow), Arduino pin 12 to microchip pin 8 (blue) and Arduino pin 13 to microchip pin 9 (orange)).

<br>

![](../images/week10/con_a.jpg)

<br>

## Preparation

To run the Code correctly I had to install a new board manager (AttinyCore). Then I had to go to "Tools" and had to select "Attiny84" under "Boards" in the new boardmanager. To install the manager I followed this instructions: 

<br>

https://github.com/SpenceKonde/ATTinyCore/blob/master/Installation.md

<br>


## Code 

I wanted to use a speaker, so I used the tone() function. First I included SoftwareSerial again, to use the FTDI-cable to print data if necessery. Then I defined RX and TX for the FTDI-cable and the speakers pin. In the setup I choosed the speakers pin as output. Then in the loop, I used the tone() function. I had to set the output pin of the speaker, the tone frequency in herzt and the duration of the tone. At the end it should make different tones which starts low go higher and lower again.

<br>

![](../images/week10/code.png)
![](../images/week10/code2.png)

<br>

## Speaker Connection

After uploading the Code to the PCB, I connected the FTDI-cable to it (the black one with GND, the red one with +5V, the orange one (RX) to the free pin (I defined as RX in the Code) and the yellow one (TX) with the free pin (I defined as TX)). Then I connected the speaker. It had two cables at it. One black and one red cable. I connected the black cable to my PCBs GND and the red one to my PCBs pinout 3. 

<br>

![](../images/week10/speaker.jpg)

<br>

After connectong the FTDI-usb to the laptop it started working:


[Download the Video show its working here](../images/week10/vid.mp4) 

[Download the Code here](../images/week10/speaker.zip)  


