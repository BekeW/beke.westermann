# 9. Input Devices

This week I worked with an LDR Light Sensor and my pcb. I needed an Arduino uno, a ftdi cable, jumper wires and a usb cable to connect the arduino to my laptop, a 10k resistor and thelight sensor (SEN-09088). I used the same Arduino as last week, so I didnt had to upload the ArduinoISP sketch or burn the pcb as bootloader.  
Again I connected the Arduino with the pcb and the laptop and uploaded the code (the Arduino GND to microchip pin 14, Arduino VCC to microchip 1, Arduino pin 10 to microchip pin 4 (PB3), Arduino pin 11 to microchip pin 7 (PA6), Arduino pin 12 to microchip pin 8 (PA5) and Arduino pin 13 to microchip pin 9 (PA4)).    
I had to make sure, the correct port, board, processor and clock under "Tools" was selected (COM6 to me, Attiny 24/44/84, Attiny84, 8MHz).  

## Sensor connection

For the sensor I used a circuit board. On the minus pool from the sensor I connected a 10k Ohm resistor. Then I connected it to GND, a free pin and the plus pool to +5V.

<br>

![](../images/week09/sensor1.jpg)
![](../images/week09/sensor2.jpg)


## Code

Then I wrote the code. I wanted to read the value of the light sensor and converted it to lumen. Therefor I first defined the pins, similar to last week, then I defined the volt value (5V) and the resistor value (10k) for the convertation later.  
I included the SoftwareSerial library to communicate and print my values. And I set the communication speed to 4500 in the void setup.  
In the loop I read the sensor value, and converted it with the official function to lumen. I printed it and if the sensor value was higher than 700 it was defined as "bright", if it was lower it was defined as "low". The sensor value is given into a function (sensorRawToPhys) which coverts it into a lumen value and return this value.


<br>

![](../images/week09/setup.png)
![](../images/week09/loop.png)
![](../images/week09/calc.png)

<br>

The Output looked like this: 

<br> 

![](../images/week09/output.png)

<br>

The  first calue shows the sensor output, the second value is the lumen. To compare: A normal lumen value in a bright room e.g. kitchen is round abut 300. In the FabLab were many windows, so it had about 300-400 lumen. When I covered the sensor with my hands, it had like 10 to 30 lumen.

<br>

![](../images/week09/sensor1.jpg)


<br>

## Download

[Download the Code here](../images/week09/sensor.zip)  
