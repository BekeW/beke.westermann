# 1. Principles and practices

This week I worked on creating and setting up my own website for the documentation of my future work. For the basic creation and administration I used Git. To create my website, I used Markdown.  
<br>
## What is Git and how does it work?
---
Git is a _version control system_ developed in 2005 by _Linus Torvalds_.  
It is an open source project and free to use. In Git you have a repository with your projectfiles in a master branch. You can create more branches as a copy from the master branch and work in these different branches. If you want to make a change to a file but you are not sure if it works fine after the change, the change is made in a special branch (created for this case e.g. develop branch) so you can test it without risking anything. So at the end Git enables working on different versions of a project in one envoronment.  
<br>

## Working with Git
---  
### Installation and set up  
To use Git it first has to be installed. I found the right Git Version (depending on your os) on the offical Git website.  
To set up Git I defined my email and username in the first steps.  
```
// Open the Git Bash
git config --global user.name "Beke Westermann"
git config --global user.email mail@beke.com
```  
<br>  

### ssh key generation and cloning
Then I generated and enabled my ssh-key.  
```
// generation of the key
ssh-keygen -t rsa -C "mail@beke.com"

// copy the key and show it
cat ~/.ssh/id_rsa.pub
```
Then I pasted the key to my Gitlab website.
So, since Git was set up, I needed to clone my project to my local projectfolder.

```
// I choosed the ssh option in the clone section
git clone git@gitlab.com:BekeW/beke.westermann.git
```  
<br>  

### Commiting and pushing a project  

Now I could open my project in vs code and work at it:

![](../images/vscode-git.png)  
<br>

When I finished my work, I had to push the changes I made to my projet back to Gitlab.
```
// First I needed to add all my changes
git add --all
```
So I see the following when I checked the Addition with 
``` 
git status
```  
<br>

Before the git add command:  
![](../images/without-add.png)   

<br>

After the git add command:  
![](../images/with-add.png)  

<br>

As last step I had to commit my changes an push them back to the repository.
```
git commit -m "week01"
git push
```
After the push there appears the following message:
```
$ git push
Enumerating objects: 19, done.
Counting objects: 100% (19/19), done.
Delta compression using up to 4 threads
Compressing objects: 100% (12/12), done.
Writing objects: 100% (12/12), 217.72 KiB | 3.07 MiB/s, done.
Total 12 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.com:BekeW/beke.westermann.git
   2990ff4..a77128f  main -> main

```
The changes were pushed successfully. I can see my last commit in my repository on the gitlab page:  
  
![](../images/commit.png)  
<br>
<br>

## What is Markdown?
---

Markdown is a lightweight markup language. It is used e.g. in blogs, online forums, instant messaging or documentation pages. There are many options to change the text form, format or color of text with Markdown. By using special characters a word can be written in **bold**, `monospace` or _italic_.  

<br> 

## Working with Markdown
---  
  
![](../images/markdown.png)  

<br>
This is what my Markdownexample looks like. It shows some of the mentioned functions. The bold,italic and monspace text above is made with **, `` or _ around the word. For linebreaks I used a double space after the last word of the first line or just wrote a br-tag. Images can be inserted  
with ![](../images/image-name).