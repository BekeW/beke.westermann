# 2. 2D and 3D Models

This week I made my first sketches for components of my final project. Therefor I used LibreCad and Fusion360.

## Project idea

The first problem was finding a project idea. I sketched a few ideas and finally I decided for a small box, with a temperature sensor, which shows if its too cold or too warm in a room. It could be used for e.g. indoor plants.

<br>

## 2D Model

<br>
The 2D model was made with LibreCAD. The Box will be made with a laser cutter, so it I will cut every single part and then put them together. Therefor I needed joints at the edges of the boxparts. It was tricky to find the fitting sizes for the joints, but at the end it worked.  

In the first steps I made a basic model of the front side of the box:  
<br>
![](../images/week02/LC1.png)

<br>

Then I added the joints for every edge. I had to change them a few times, beacause the length of the boxes changed.

<br>   

![](../images/week02/LC2.png)  

<br>  

Then I made the last parts. I just could copy the part of the sides and bottom/top.  
<br>

![](../images/week02/LC4.png)

<br>  

At the end I had three parts, I used an extra Layer (in yellow) to describe the part size in centimeter.  
<br>

![](../images/week02/LC5.png) 

![](../images/week02/LC6.png)  

<br>

[Download here](../projects/Sketch.dxf)


<br>

## 3D Model

After I finished the 2D model with LibreCAD, I realizes the 2D box with Fusion360 in 3D.
Firsty I drew a sketch, similar to the first steps in LibreCAD:  

<br>

![](../images/week02/3D(1).png)  

<br> 

Then I added the joints and repeated the steps for the rest of the parts.  

<br>

![](../images/week02/3D(2).png)
![](../images/week02/3D(3).png)

<br>

After that I used the Extrusion tool to turn the sketches into 3D:  

<br>

![](../images/week02/3D(4).png)  

<br> 

Again I repeated the step for every part. At the end I had all four parts in 3D:  

<br>

![](../images/week02/3D(5).png) 

<br>

As last step I put the 3D parts together, so I had the first part of my project:  

<br>

![](../images/week02/3D(6).png)

<br>

![](../images/week02/3D(7).png)

<br>


[Download here](../projects/Box v1.f3d)


