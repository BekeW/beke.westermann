# Final Project

## Project Idea and Concept 

In the last few weeks I worked at my final project. I had a few ideas at the beginning of the course: An LED clock, a temperature mesaure tool for plants or a small weather station. And I decided for the weather station. 
The first few designs where very different. It should be a box, with joints to hold it togethter, but I wasn´t sure how it should look at the end. 


At the end  chose the display-design. Because I used a humidity sensor, I could not only display the temperature, but the humditiy and the temperature in Fahrenheit. For the display I used an OLED display. It had only a few connection pins, so my board wouldn´t have that much. 

## PCB Design

For the pcb I changed a given pcb with a ATMEGA328P. You can find both pcbs (the original pcb and my changed pcb) in the download section.
I removed a few pinouts, LEDs and buttons. I kept the vcc and ground part and added a few pins for them. I also kept the LED to test the blink-sketch from the Arduino examples to test the pcb at the end and I kept the crystal for the 16Mhz. I had to connect the pins I needed. Besides vcc and ground, I needed an Out-Pin for the humidity sensor, SCL and SCK for the OLED display, RX and TX for the FTDI-cable and the pins for the Arduino connection to upload my code (MISO, MOSI, VCC, GND and RESET).
The Design looked like this at the end: 

<br>

![](../images/finalProject/pcbdesign.png)

<br>

After assigning the footprints to the schematic, I loaded the pcb layout for printing and arrange the different parts. It was a little tricky and I had to put the connection from c5 between c1 and c2, but the other connections were fine.  
The final layout:

<br>

![](../images/finalProject/pcblayout.png)

<br>


## Exporting the pcb Design  

To Cut the designed pcb, I firstly had to export it as svg file:  

<br>

![](../images/week07/exportkicad.png)  

<br>

In the next step I opened the svg File in Inkscape and exported it as png file. 

<br>

![](../images/finalProject/all.png)  


<br>

After these steps, i opened the above png Image in GIMP. For the margin, I changed the settings of the Canvas size. I added 3 mm on the top/bottom and on the sides. Then I prepared the image for the needed three parts (traces, holes and cutout). I changed the color of the traces and the holes to black, so I just had a white margin. I saved this as png File as cutout. And respeated this step with the traces and the holes. The part, which would be cut out, had to be white. 

<br>

![](../images/finalProject/traces.png)  

![](../images/finalProject/cutout.png)  

![](../images/finalProject/holes.png)  

<br>

Then I had to set up the parameters for the cutting process in fabmodules.org. 
Here I selected the input format "png image" (selected the images from above), the output format "rml file", and set the rest. For the traces I had to invert the image and select "PCB traces (1/64)" with x0, y0, z0 all to 0, cut depth to 0, offset number to 8. For the cutout and the holes, i also had to invert the images an set "PCB outline (1/32)", cut depth to  0,5 and press on "calculate". For all three Setups, I had to select "MDX-40" as maschiene. 
Then I could save the three parts as .rml files (also in the downlaod section) and started with the next step.

<br>


## Cutting process

For the cutting process I turned on the vacuum and placed the pcb material on it . I swaped the endmill to the one with 3,175 mm. Then I set the x und y axis as origin and after that the z axis. Therefor I went down on the axis step for step until the endmill touched the pcb. Once it was set, I selected the .rml file from the usb stick (the traces first, then the holes and the coutout at the end) and started the cutting process.
After the first cutting process, I changed the endmill (now changed to 3,9688 mm) and started the cutting process for the holes. I repeated this step for the cutout. 
Here´s the cutting process for the traces:

<br>

![](../images/finalProject/cut1.jpg)  

<br>

![](../images/finalProject/cut1.jpg)  

<br>

Then I had to solder all the part tho my pcb. It took a while, I had a few problems to solder the ATMEGA and destroyed trace parts of the chip... 
After the soldering process it looked fine (I had to solder a alternativ trace part to the bottom right of the chip, so it had a connection to SCK):

<br>

![](../images/finalProject/finalpcb.jpg)  

<br>

## Programming

To be able to upload a programm to my chip I first had to open the AurduinoISP Sketch (can be found under File>Examples>ArduinoISP). Then I connected the Arduino to my Laptop and selected the correct port, board and programmer unter "tools" (COM6 to me, Arduino uno and Arduino as ISP). Then I uploaded the sketch to the Arduino and disconnected it.  
In the next step I had to connect the Arduino uno to my pcb. Therefor I used th follwing image as an orientation:

<br>

![](../images/week08/arduino_connection.png)

<br>

I conected the jumper wires to the Arduino as it is shown in the picture. But I had a different pcb, so I looked up the datasheet for my microcontroller (ATMEGA328P), and looked for the pins (had so search for the pins MISO, MOSI, RST,VCC and SCK). 

<br>

![](../images/finalProject/datasheet.png)

<br>

As soon as it was connected, I selected the correct port and board under "Tools" (COM6 and Arduino Uno, because they have the same microchip). Then I selected "Burn bootloader" under "Tools".   
Now I could upload the blink sketch to test my board. 
The blink sketch can be found under File > Example > Basics > Blink. It worked fine:

<br>

![](../images/finalProject/video.gif)


<br>

Then I could try to write the project-code and upload it. The code was actually simple. It should use the humidity sensor to mesure the humidity and temperature and then it should display the results on the display.
That´s the code (also in the Download section):

<br>

![](../images/finalProject/code1.png)


<br>

Here I imported the libraries I needed for the sensor and the OLED. Then I defined th variables fpr the temperature in Celsius and Fahrenheit and for the humidity. I also defined some variables for the OLED screen.

<br>

![](../images/finalProject/code2.png)

<br>

In the set up  I started the Serial communication and told the display to show the Adafruit (imported library) logo. Then it should clear itself.

<br>

![](../images/finalProject/code3.png)

<br>

![](../images/finalProject/code4.png)

<br>

![](../images/finalProject/code5.png)

<br>

The last code snippets write the data from the humditiy sensor into the variables and the display shows them in different sizes. 

<br>

![](../images/finalProject/nan.jpg)

<br>

A few problems with the wrong connections...   :/

<br>

![](../images/finalProject/code5.png)

<br>

Then it worked fine! :>

<br>

## Laser cutting the box for the weather station

So now I did the Electronics and my pcb worked, I needed a kind ob box for it. Therefor I designed a wood box with joints and holes for the display, the sensor and the cables.
Top/Bottom and Left/Right 7cm width, 10cm heigth, 0,7cm joint depth, at the heigth side 1cm and 0,5cm joint width, at width side 0,7cm joint width. 
Front/Back 10cm width and height, joints depth is 0,5cm, and joints width is 0,5cm and 1cm.
The sketches:

<br>

![](../images/finalProject/sides.png)

<br>

![](../images/finalProject/topbottom.png)

<br>

![](../images/finalProject/front.png)

<br>

![](../images/finalProject/back.png)

<br>

Then I saved all the files as dfx on my usb stick and went over to the laser cutter.
I used 6mm thick wood as material, put it in the laser cutter and set the heigth and the origin point.
Then I imported my .dfx-Files in the program. I had a few scaling problems, but I could solve them. The scaling wasn´t 1:1, so it would cut the parts too big.
The parts got all the red cutting layer, because there was just one part to cut, except the front and back parts. Here I gave the inner holes the red layer, to cut it first. 

The final parts:

<br>

![](../images/finalProject/parts.jpg)

<br>

Now I could stick them together:

<br>

![](../images/finalProject/p1.jpg)

<br>

![](../images/finalProject/p2.jpg)

<br>

![](../images/finalProject/p3.jpg)

<br>

![](../images/finalProject/p4.jpg)

<br>

Then I put the electronics in it:

<br>

![](../images/finalProject/boxelec.jpg)

<br>

It was really difficult to sort the electronics in there, I needed something to seperate and to put them in order. So in the next step I 3D printed the part I needed for this!

<br>

## 3D Printing the Inside of the box

I wanted to print a smal kind of table to put the pcb on it, so the pcb and the sensor were seperated. The table also needed a hole for the cables. So the first sketch looked like this:

<br>

![](../images/finalProject/3d1.png)

<br>

The parts at the corners are the feets of the table. I extruded the sketch (press e) and added a hole:

<br>

![](../images/finalProject/3d2.png)

<br>

then I saved the file as .ufp-File and opened it in ultimaker. I selected the printer I wanted to use (Ultimaker S5) and worked with the following settings:

<br>

![](../images/finalProject/setting1.png)

<br>

![](../images/finalProject/setting2.png)

<br>

![](../images/finalProject/setting3.png)

<br>

It looked like this:

<br>

![](../images/finalProject/utimaker1.png)

<br>

I also needed support, so in the preview it showed me how it would be printed:

<br>

![](../images/finalProject/utimakerpreview.png)

<br>

I saved this File on my usb stick. Then I connected the stick to the printer, chose my file and started printing.
It took round about 4h to print it, the result was fine: 

<br>

![](../images/finalProject/1.jpg)

<br>

![](../images/finalProject/2.jpg)

<br>

![](../images/finalProject/3.jpg)

<br>

![](../images/finalProject/3d.jpg)

<br>

At the end I put everything together. I put the electronics in the box in order with the help of the mini-table and drilled down the display to my box. As I finished the result looked like this: 

<br>

![](../images/finalProject/box.jpg)

<br>

![](../images/finalProject/box2.jpg)



<br>

Now I just had to spike the display to the front side of the box:

<br>

![](../images/finalProject/spike.jpg)

<br>

Now connect everything again, and it works!

<br>

![](../images/finalProject/end.jpg)

<br>

![](../images/finalProject/end2.jpg)

<br>

## Videos

<br>

[Watch Making-Of Video here](../images/finalProject/vid1.mp4)  
[Watch Final Video here](../images/finalProject/vid2.mp4)  

<br>



## Downloads

The originial pcb: https://github.com/satshakit/satshakit  
[Download PCB-Data here](../images/finalProject/final_project_pcb_data.zip) 

<br>

[Download all here](../images/finalProject/all.png)  
[Download Traces here](../images/finalProject/traces.png)  
[Download Holes here](../images/finalProject/holes.png)  
[Download Cutout here](../images/finalProject/cutout.png)  

<br>

[Download Side Box Parts here](../images/finalProject/Right.dxf)  
[Download Side Box Parts here](../images/finalProject/Left.dxf)  
[Download Top/Bottom Box Parts here](../images/finalProject/Top.dxf)  
[Download Front Box Part here](../images/finalProject/Front2.dxf)  
[Download Back Box Part here](../images/finalProject/Back2.dxf)  

<br>

[Download Box Inside Fusion File here](../images/finalProject/UMS5_finalProjectInside.ufp)  
[Download Box Inside Ultimaker File here](../images/finalProject/finalProjectInside.stl)  

[Download Code here](../images/finalProject/project2.ino)  







