EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12447 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4100 4600 3900 4600
Wire Wire Line
	2800 3500 2500 3500
Wire Wire Line
	900  3000 1200 3000
Wire Wire Line
	1200 3000 1700 3000
Wire Wire Line
	2200 3000 1700 3000
Wire Wire Line
	1700 2700 1700 3000
Wire Wire Line
	1200 2700 1200 3000
Wire Wire Line
	2200 2700 2200 3000
Connection ~ 1700 3000
Connection ~ 1200 3000
Text Label 900  3000 0    10   ~ 0
VCC
Wire Wire Line
	4100 4100 3300 4100
Wire Wire Line
	3300 4100 2600 4100
Wire Wire Line
	2600 4100 2600 3800
Connection ~ 3300 4100
Wire Wire Line
	4100 4300 3300 4300
Wire Wire Line
	3300 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4600
Connection ~ 3300 4300
Wire Wire Line
	900  2100 1200 2100
Wire Wire Line
	1200 2100 1700 2100
Wire Wire Line
	1700 2100 2200 2100
Wire Wire Line
	1200 2400 1200 2100
Wire Wire Line
	1700 2400 1700 2100
Wire Wire Line
	2200 2400 2200 2100
Connection ~ 1200 2100
Connection ~ 1700 2100
Text Label 900  2100 0    10   ~ 0
GND
Wire Wire Line
	4100 5700 3900 5700
Wire Wire Line
	2300 3800 2100 3800
Wire Wire Line
	2300 4600 2100 4600
Wire Wire Line
	4100 5500 3900 5500
Wire Wire Line
	2100 6600 2000 6600
Wire Wire Line
	2600 6600 2500 6600
Wire Wire Line
	4100 3500 3650 3500
$Comp
L satshakit_cnc-rescue:VCC-satshakit_cnc-eagle-import #P+07
U 1 1 FAFE20BD
P 800 3000
F 0 "#P+07" H 800 3000 50  0001 C CNN
F 1 "VCC" V 700 2900 59  0000 L BNN
F 2 "" H 800 3000 50  0001 C CNN
F 3 "" H 800 3000 50  0001 C CNN
	1    800  3000
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-rescue:CSM-7X-DU-satshakit_cnc-eagle-import CRYSTAL1
U 1 1 99621192
P 3300 4200
F 0 "CRYSTAL1" H 3400 4240 59  0000 L BNN
F 1 "16Mhz" H 3400 4100 59  0000 L BNN
F 2 "crystal_eagle:01_FAB_HELLO_CSM-7X-DU" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-rescue:0612ZC225MAT2A-satshakit_cnc-eagle-import C1
U 1 1 7EE2FFE3
P 2300 3800
F 0 "C1" H 2264 3909 69  0000 L BNN
F 1 "22pF" H 2225 3492 69  0000 L BNN
F 2 "FAB:C_1206" H 2300 3800 50  0001 C CNN
F 3 "" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-rescue:0612ZC225MAT2A-satshakit_cnc-eagle-import C2
U 1 1 1A664D53
P 2300 4600
F 0 "C2" H 2264 4709 69  0000 L BNN
F 1 "22pF" H 2225 4292 69  0000 L BNN
F 2 "FAB:C_1206" H 2300 4600 50  0001 C CNN
F 3 "" H 2300 4600 50  0001 C CNN
	1    2300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-rescue:GND-satshakit_cnc-eagle-import #GND01
U 1 1 8DA2BB24
P 800 2100
F 0 "#GND01" H 800 2100 50  0001 C CNN
F 1 "GND" H 700 2000 59  0000 L BNN
F 2 "" H 800 2100 50  0001 C CNN
F 3 "" H 800 2100 50  0001 C CNN
	1    800  2100
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-rescue:RESISTOR1206-satshakit_cnc-eagle-import R1
U 1 1 5FC72806
P 3000 3500
F 0 "R1" H 2850 3559 59  0000 L BNN
F 1 "10k" H 2850 3370 59  0000 L BNN
F 2 "FAB:R_1206" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-rescue:LED1206-satshakit_cnc-eagle-import LED_13
U 1 1 D663D747
P 2700 6600
F 0 "LED_13" V 2840 6520 59  0000 L BNN
F 1 "YELLOW" V 2925 6520 59  0000 L BNN
F 2 "FAB:LED_1206" H 2700 6600 50  0001 C CNN
F 3 "" H 2700 6600 50  0001 C CNN
	1    2700 6600
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-rescue:RESISTOR1206-satshakit_cnc-eagle-import R3
U 1 1 4F7F9F8C
P 2300 6600
F 0 "R3" H 2150 6659 59  0000 L BNN
F 1 "499" H 2150 6470 59  0000 L BNN
F 2 "FAB:R_1206" H 2300 6600 50  0001 C CNN
F 3 "" H 2300 6600 50  0001 C CNN
	1    2300 6600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-rescue:CAP1206-satshakit_cnc-eagle-import C3
U 1 1 C3EA450F
P 2200 2500
F 0 "C3" H 2260 2615 59  0000 L BNN
F 1 "10uF" H 2260 2415 59  0000 L BNN
F 2 "FAB:C_1206" H 2200 2500 50  0001 C CNN
F 3 "" H 2200 2500 50  0001 C CNN
	1    2200 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-rescue:CAP1206-satshakit_cnc-eagle-import C4
U 1 1 674C1B16
P 1700 2500
F 0 "C4" H 1760 2615 59  0000 L BNN
F 1 "1uF" H 1760 2415 59  0000 L BNN
F 2 "FAB:C_1206" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-rescue:CAP1206-satshakit_cnc-eagle-import C5
U 1 1 8E04CE4C
P 1200 2500
F 0 "C5" H 1260 2615 59  0000 L BNN
F 1 "100nF" H 1260 2415 59  0000 L BNN
F 2 "FAB:C_1206" H 1200 2500 50  0001 C CNN
F 3 "" H 1200 2500 50  0001 C CNN
	1    1200 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 5800 3900 5800
Wire Wire Line
	4100 5000 3900 5000
Wire Wire Line
	4100 4700 3900 4700
$Comp
L satshakit_cnc-rescue:ATMEGA48_88_168-AU-satshakit_cnc-eagle-import MICRO1
U 1 1 B7F66DC5
P 5300 4600
F 0 "MICRO1" H 4300 5900 59  0000 L TNN
F 1 "ATMEGA328P-AU" H 4300 3200 59  0000 L BNN
F 2 "FAB:TQFP-32_7x7mm_P0.8mm" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4900 3900 4900
Wire Wire Line
	6850 5800 6500 5800
Wire Wire Line
	6500 5700 6850 5700
Wire Wire Line
	6500 5600 6850 5600
Wire Wire Line
	6850 4600 6500 4600
Wire Wire Line
	6500 4500 6850 4500
Wire Wire Line
	6500 4400 6850 4400
Wire Wire Line
	6500 3900 6900 3900
Wire Wire Line
	6500 4000 6900 4000
Text GLabel 9500 3300 0    50   Input ~ 0
VCC
Text GLabel 9500 3400 0    50   Input ~ 0
VCC
Text GLabel 9500 3500 0    50   Input ~ 0
VCC
Text GLabel 9500 3600 0    50   Input ~ 0
VCC
Text GLabel 9500 3700 0    50   Input ~ 0
GND
Text GLabel 9500 3800 0    50   Input ~ 0
GND
Text GLabel 9500 3900 0    50   Input ~ 0
GND
Text GLabel 9500 4000 0    50   Input ~ 0
GND
Text GLabel 9500 4100 0    50   Input ~ 0
GND
Wire Wire Line
	9500 3300 9700 3300
Wire Wire Line
	9700 3400 9500 3400
Wire Wire Line
	9500 3500 9700 3500
Wire Wire Line
	9700 3600 9500 3600
Wire Wire Line
	9500 3700 9700 3700
Wire Wire Line
	9700 3800 9500 3800
Wire Wire Line
	9500 3900 9700 3900
Wire Wire Line
	9700 4000 9500 4000
Wire Wire Line
	9500 4100 9700 4100
Text GLabel 3900 4600 0    50   Input ~ 0
VCC
Text GLabel 3900 4700 0    50   Input ~ 0
VCC
Text GLabel 3900 4900 0    50   Input ~ 0
VCC
Text GLabel 3900 5000 0    50   Input ~ 0
VCC
Text GLabel 3900 5500 0    50   Input ~ 0
GND
Text GLabel 3900 5700 0    50   Input ~ 0
GND
Text GLabel 3900 5800 0    50   Input ~ 0
GND
Text GLabel 2100 3800 0    50   Input ~ 0
GND
Text GLabel 2100 4600 0    50   Input ~ 0
GND
Text GLabel 2000 6600 0    50   Input ~ 0
GND
Text GLabel 2500 3500 0    50   Input ~ 0
VCC
Wire Wire Line
	2900 6600 6500 6600
Wire Wire Line
	6500 6600 6500 5800
Connection ~ 6500 5800
Wire Wire Line
	6500 3500 6900 3500
Wire Wire Line
	6500 3600 6900 3600
Wire Wire Line
	6900 3700 6500 3700
Wire Wire Line
	6500 3800 6900 3800
Text GLabel 3650 3250 1    50   Input ~ 0
RESET
Wire Wire Line
	3650 3250 3650 3500
Connection ~ 3650 3500
Wire Wire Line
	3650 3500 3200 3500
Text GLabel 8200 5050 0    50   Input ~ 0
RESET
$Comp
L FAB:Conn_PinHeader_1x09_P2.54mm_Vertical_THT_D1.4mm J6
U 1 1 61C30EFB
P 9900 3700
F 0 "J6" H 9872 3632 50  0000 R CNN
F 1 "Conn_PinHeader_1x09_P2.54mm_Vertical_THT_D1.4mm" H 9872 3723 50  0000 R CNN
F 2 "eagle_pin:09P" H 9900 3700 50  0001 C CNN
F 3 "~" H 9900 3700 50  0001 C CNN
	1    9900 3700
	-1   0    0    1   
$EndComp
$Comp
L FAB:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm J5
U 1 1 61C394DB
P 8750 5150
F 0 "J5" H 8722 5082 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 8722 5173 50  0000 R CNN
F 2 "eagle_pin:03P" H 8750 5150 50  0001 C CNN
F 3 "~" H 8750 5150 50  0001 C CNN
	1    8750 5150
	-1   0    0    1   
$EndComp
$Comp
L FAB:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm J2
U 1 1 61C3C3F0
P 7050 5700
F 0 "J2" H 7022 5632 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 7022 5723 50  0000 R CNN
F 2 "eagle_pin:03P" H 7050 5700 50  0001 C CNN
F 3 "~" H 7050 5700 50  0001 C CNN
	1    7050 5700
	-1   0    0    1   
$EndComp
$Comp
L FAB:Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm J1
U 1 1 61C3DBF2
P 7050 4500
F 0 "J1" H 7022 4432 50  0000 R CNN
F 1 "Conn_PinHeader_1x03_P2.54mm_Vertical_THT_D1.4mm" H 7022 4523 50  0000 R CNN
F 2 "eagle_pin:03P" H 7050 4500 50  0001 C CNN
F 3 "~" H 7050 4500 50  0001 C CNN
	1    7050 4500
	-1   0    0    1   
$EndComp
$Comp
L FAB:Conn_PinHeader_1x04_P2.54mm_Vertical_THT_D1.4mm J3
U 1 1 61C3F09C
P 7100 3700
F 0 "J3" H 7072 3582 50  0000 R CNN
F 1 "Conn_PinHeader_1x04_P2.54mm_Vertical_THT_D1.4mm" H 7072 3673 50  0000 R CNN
F 2 "eagle_pin:04P" H 7100 3700 50  0001 C CNN
F 3 "~" H 7100 3700 50  0001 C CNN
	1    7100 3700
	-1   0    0    1   
$EndComp
$Comp
L FAB:Conn_PinHeader_1x02_P2.54mm_Vertical_THT_D1.4mm J4
U 1 1 61C4000C
P 7100 4000
F 0 "J4" H 7072 3882 50  0000 R CNN
F 1 "Conn_PinHeader_1x02_P2.54mm_Vertical_THT_D1.4mm" H 7072 3973 50  0000 R CNN
F 2 "eagle_pin:02P" H 7100 4000 50  0001 C CNN
F 3 "~" H 7100 4000 50  0001 C CNN
	1    7100 4000
	-1   0    0    1   
$EndComp
Text GLabel 2650 3000 2    50   Input ~ 0
VCC
Wire Wire Line
	2650 3000 2200 3000
Connection ~ 2200 3000
Text GLabel 2600 2100 2    50   Input ~ 0
GND
Wire Wire Line
	2600 2100 2200 2100
Connection ~ 2200 2100
Wire Wire Line
	8200 5050 8450 5050
Wire Wire Line
	8250 5250 8550 5250
$Comp
L satshakit_cnc-rescue:CAP1206-satshakit_cnc-eagle-import C6
U 1 1 61BCA7E7
P 8050 5250
F 0 "C6" H 8110 5365 59  0000 L BNN
F 1 "100nF" H 8110 5165 59  0000 L BNN
F 2 "FAB:C_1206" H 8050 5250 50  0001 C CNN
F 3 "" H 8050 5250 50  0001 C CNN
	1    8050 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 5250 7700 5250
Wire Wire Line
	7700 5250 7700 4900
Wire Wire Line
	7700 4900 8450 4900
Wire Wire Line
	8450 4900 8450 5050
Connection ~ 8450 5050
Wire Wire Line
	8450 5050 8550 5050
$EndSCHEMATC
